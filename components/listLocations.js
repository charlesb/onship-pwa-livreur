import React from 'react'
import Autocomplete from 'react-google-autocomplete'
import { useSelector,useDispatch,connect } from 'react-redux'

const UserAddressPrompt = (props) => {
  const userAddress = useSelector(state => state.userAddress)
  const dispatch = useDispatch()
  let modalClass;
  if (userAddress.address == undefined){
    modalClass = 'modal is-active';
  }else{
    return <div></div>
  }
  return (
    <div className={modalClass}>
      <div className="modal-background"></div>
      <div className="modal-content">
          <div className="box">
              <h2 className="title is-size-3 has-text-centered">O&ugrave; &ecirc;tes vous?</h2>
              <div className="has-text-centered">
                <label htmlFor="user_address" className="label">Pour consulter la liste des commerces &agrave; proximit&eacute;, entrez votre adresse ci-dessous.</label>
                <Autocomplete
                    style={{width: '80%'}}
                    className="input"
                    id="user_address"
                    onPlaceSelected={(place) => {
                      var user_address = {
                        address: place.address_components[0].long_name + " " + place.address_components[1].long_name,
                        city: place.address_components[2].long_name,
                        postal_code: place.address_components[6].long_name,
                        latitude: place.geometry.location.lat(),
                        longitude: place.geometry.location.lng(),
                      }
                      dispatch({
                        type: 'CHANGE_ADDRESS',
                        userAddress: user_address
                      })
                      localStorage.setItem('userAddress', JSON.stringify(user_address));
                      props.callback(user_address)
                    }}
                    types={['address']}
                    componentRestrictions={{country: "ca"}}
                />
              </div>
              <br/>
              <p id="modal-error-message" className="has-text-danger error-message has-text-centered"></p>
              <p className="has-text-centered">
                  <button id="send-email-products" className="button is-primary">
                      Soumettre
                  </button>
              </p>
          </div>
      </div>
      <button className="modal-close is-large" aria-label="close"></button>
    </div>
  )
}

function LocationSkeleton(props) {
  return (
    <div className="column is-3-desktop is-4-tablet is-6-mobile">
      <div className="card location-card loading">
        <div className="card-image">
        </div>
      </div>
    </div>
  );
}

function LoadingLocations(props) {
  return (
    <div className="columns is-multiline is-mobile">
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
      <LocationSkeleton></LocationSkeleton>
    </div>
  );
}

function LocationsList(props) {
  return (
    <div className="columns is-multiline is-mobile">
      {props.locations.map((location) => (
        <a key={location.id_location} href="#" className="column is-4-desktop is-4-tablet is-6-mobile">
          <div className="card location-card">
            <div className="card-image">
                <img src={process.env.api_url + location.img_logo} alt={location.name} />
            </div>
            <div className="tags">
              <span id={'location-' + location.id_location} className="tag is-primary is-light placeholder-item">
                <p>
                  <span id={'delivery-estimate-' + location.id_location}></span>
                </p>
              </span>
            </div>
          </div>
        </a>
        ))
      }
    </div>
  );
}

class ListLocations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userAddress: props.userAddress,
      locationsFetched: false,
      apiUrl: process.env.API_URL,
      locations: []
    };
    this.fetchLocations = this.fetchLocations.bind(this);
  }

  componentDidMount () {
    if (this.state.userAddress.address != undefined){
      fetch(process.env.api_url + '/api/location/get-nearby-locations/' + this.state.userAddress.latitude + '/' + this.state.userAddress.longitude)
      .then(response => response.json())
      .then(data => this.setState({...this.state, locationsFetched: true, locations: data.locations}));
    }
  }

  fetchLocations(userAddress) {
    fetch(process.env.api_url + '/api/location/get-nearby-locations/' + userAddress.latitude + '/' + userAddress.longitude).then(
      res => this.setState({...this.state, locationsFetched: true, locations: res.locations})
    )
  }

  render () {
    let locations;
    if (this.state.locations.length > 0){
      locations = <LocationsList locations={this.state.locations}></LocationsList>
    }else{
      locations = <p>Aucun commerce &agrave; proximit&eacute;</p>
    }
    
    return (
      <div>
        {this.state.locationsFetched &&          
          <div>{locations}</div>
        }
        {!this.state.locationsFetched &&
          <LoadingLocations></LoadingLocations>
        }
        <UserAddressPrompt callback={this.fetchLocations}></UserAddressPrompt>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    userAddress: state.userAddress,
  }
}
export default connect(mapStateToProps)(ListLocations)