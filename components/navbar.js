import React from 'react'
import Link from 'next/link'
import { useSelector } from 'react-redux'

const UserAddressComponent = () => {
    const userAddress = useSelector(state => state.userAddress)
    if (userAddress.address != undefined){
        return <div className="navbar-item">Votre adresse:&nbsp;<b className="clickable-link">{userAddress.address}</b></div>
    }else{
        return <div className="navbar-item"></div>
    }
}

export default class Navbar extends React.Component {
    
  constructor(props) {
    super(props);
    this.state = {
        isLoggedIn: false,
    };
  }

  render () {
    return (
      <nav className="navbar is-fixed-top has-shadow">
        <div className="container">
          <div className="navbar-brand">
            <Link href="/">
                <a className="navbar-item" href="#"><img src="/imgs/logo.png" alt="" width="auto" style={{minHeight: "40px"}} /></a>
            </Link>
            
            <a className="navbar-burger" role="button" type="button">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <div className="navbar-menu">
            <div className="navbar-start">
                <UserAddressComponent></UserAddressComponent>
            </div>
            <div className="navbar-end">
              <div className="navbar-item">
                <div className="control has-icons-right">
                  <input className="input" type="text" placeholder="Rechercher un produit/commerce" style={{minWidth: "300px"}} />
                  <span className="icon is-right">
                    <i className="fontello icon-search"></i>
                  </span>
                </div>
              </div>
              <div className="navbar-item">
                <button id="cart-button" className="button is-primary is-light bd-navbar-icon is-hidden-touch">
                  <div className="text-icon"><i className="fontello icon-cart"></i>&nbsp;Panier&nbsp;(<span id="items-count">0</span>)</div>
                </button>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}