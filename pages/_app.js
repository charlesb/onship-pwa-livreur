import Head from 'next/head'
import '../styles/globals.scss'
import Navbar from '../components/navbar'
import { Provider } from 'react-redux'
import { useStore } from '../utils/store'

export default function MyApp({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState)

  return (
    <Provider store={store}>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <meta name="description" content="Description" />
        <meta name="keywords" content="Keywords" />
        <title>OnShip.ca - Logistique</title>

        <link rel="manifest" href="/manifest.json" />
        <link
          href="/icons/favicon-16x16.png"
          rel="icon"
          type="image/png"
          sizes="16x16"
        />
        <link
          href="/icons/favicon-32x32.png"
          rel="icon"
          type="image/png"
          sizes="32x32"
        />
        <meta name="apple-mobile-web-app-status-bar-style" content="default"></meta>
        <link rel="apple-touch-icon" href="/apple-icon.png"></link>
        <meta name="theme-color" content="#2e0df7" />
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5Yo1I1reClUx1D6H3TMUnRA3ENzUkLPQ&libraries=places"></script> 
      </Head>
      <Navbar></Navbar>
      <Component {...pageProps} />
    </Provider>
  )
}