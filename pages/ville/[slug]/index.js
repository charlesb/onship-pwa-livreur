import Link from 'next/link'

function Ville({ city }) {
  return (
    <div className="container">
        {city.name}
    </div>
  )
}

// This function gets called at build time
export async function getStaticPaths() {
    // Call an external API endpoint to get posts
    const res = await fetch(process.env.API_URL + '/api/city/get-all-slug')
    const data = await res.json()
  
    // Get the paths we want to pre-render based on posts
    const paths = data.cities.map((city) => ({
      params: { slug: city.slug },
    }))
  
    // We'll pre-render only these paths at build time.
    // { fallback: false } means other routes should 404.
    return { paths, fallback: false }
  }
  
  // This also gets called at build time
  export async function getStaticProps({ params }) {
    // params contains the post `id`.
    // If the route is like /posts/1, then params.id is 1
    const res = await fetch(process.env.API_URL + '/api/city/get/' + params.slug)
    const data = await res.json()
    const city = data.city
  
    // Pass post data to the page via props
    return { props: { city } }
  }

export default Ville