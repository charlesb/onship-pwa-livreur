import Link from 'next/link'

function Commerce({ location, city }) {
  return (
    <div>
      <div className="container">
          <nav className="breadcrumb" aria-label="breadcrumbs">
              <ul>
                  <li>
                      <Link href="/">
                        <a>Accueil</a>
                      </Link>
                  </li>
                  <li>
                      <Link href="/commerces">
                        <a>Commerces</a>
                      </Link>
                  </li>
                  <li>
                      <Link href={'/ville/' + city.slug}>
                        <a>{city.name}</a>
                      </Link>
                  </li>
                  <li className="is-active">
                      <a href="#" aria-current="page">
                        {location.name}
                      </a>
                  </li>
              </ul>
          </nav>
      </div>
      <section className="hero-category" style={{position: "relative"}}>
          <div className="container">
            <img src={process.env.API_URL + location.img_slug} />
              <h1 className="title">
                {location.name}
              </h1>
              
                {
                  location.slogan && 
                  <p>{location.slogan}</p>
                }
                {
                  !location.slogan && 
                  <p>Boutique en ligne</p>
                }
              <div className="tags">
                <span id="delivery-message-tag-desktop" className="tag is-primary is-light placeholder-item" style={{minHeight: "30px", fontSize: "0.9em"}}><p id="delivery-message-desktop" className="is-hidden"><i className="fontello icon-clock"></i> Livrer</p></span>
                {
                  location.free_shipping_amount && 
                  <span className="tag is-primary is-light" style={{minHeight: "30px", fontSize: "0.9em"}}>
                    <p>
                        Livraison gratuite &agrave; partir de {location.free_shipping_amount}$
                      </p>
                  </span>
                }
              </div>
          </div>
      </section>
    </div>
  )
}

// This function gets called at build time
export async function getStaticPaths() {
    // Call an external API endpoint to get posts
    const res = await fetch(process.env.API_URL + '/api/location/get-all-slug')
    const data = await res.json()

    // Get the paths we want to pre-render based on posts
    const paths = data.locations.map((location) => ({
      params: { slug: location.city_slug + '_' + location.slug },
    }))
  
    // We'll pre-render only these paths at build time.
    // { fallback: false } means other routes should 404.
    return { paths, fallback: false }
  }
  
  // This also gets called at build time
  export async function getStaticProps({ params }) {
    // params contains the post `id`.
    // If the route is like /posts/1, then params.id is 1
    let location_param = params.slug.split("_");

    const res = await fetch(process.env.API_URL + '/api/location/get/' + location_param[0] + '/' + location_param[1])
    const data = await res.json()
    const location = data.location
    const city = data.city
  
    // Pass post data to the page via props
    return { props: { location, city } }
  }

export default Commerce