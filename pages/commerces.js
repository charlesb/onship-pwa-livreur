import Link from 'next/link'
import ListLocations from '../components/listLocations'

function Commerces() {
  return (
    <div className="container">
        <div className="columns expedier-marche has-text-centered is-multiline is-mobile">
            <div className="column is-half-mobile">
                <a href="#" className="box">
                <img src="/imgs/onship-categorie-restaurant.svg" />
                <br/>
                <b>Restaurants</b>
                </a>
            </div>
            <div className="column is-half-mobile">
                <a href="#" className="box">
                <img src="/imgs/onship-categorie-epicerie.svg" />
                <br/>
                <b>&Eacute;piceries</b>
                </a>
            </div>
            <div className="column is-half-mobile">
                <a href="#" className="box">
                <img src="/imgs/onship-categorie-cafe-patisserie.svg" />
                <br/>
                <b>Caf&eacute; & P&acirc;tisseries</b>
                </a>
            </div>
            <div className="column is-half-mobile">
                <a href="#" className="box">
                <img src="/imgs/onship-categorie-fleuriste.svg" />
                <br/>
                <b>Fleuriste</b>
                </a>
            </div>
        </div>
        <hr/>
        <ListLocations></ListLocations>
    </div>
  )
}

export default Commerces