import Link from 'next/link'
import Autocomplete from 'react-google-autocomplete';
import { useDispatch } from 'react-redux'

export async function getStaticProps(context) {
  const res = await fetch(process.env.API_URL + '/api/location/get-new-locations')
  const props = await res.json()

  if (!props) {
    return {
      notFound: true,
    }
  }

  return {
    props: { props }, // will be passed to the page component as props
  }
}
export default function Home({ props }) {
  const dispatch = useDispatch()
  return (
    <div className="has-background-white">
      <section style={{backgroundImage: "url('/metis-assets/backgrounds/intersect.svg')", backgroundPosition: "top", backgroundRepeat: "no-repeat", backgroundSize: "contain"}}>
        <div className="section">
          <div className="container">
            <div className="is-max-w-lg mx-auto has-text-centered mb-6" style={{maxWidth: "700px", margin: "auto"}}>
              <h1 className="title is-size-2 is-spaced">
                On 💙 nos commer&ccedil;ants de quartier
              </h1>
              <p className="subtitle">On vous livre votre &eacute;picerie, restaurant, fleuriste, pâtisseries, animaleries et bien plus. Commandez et recevez aussit&ocirc;t. Votre premi&egrave;re livraison est gratuite!</p>
              
              <label htmlFor="user_address" className="label">Pour consulter la liste des commerces &agrave; proximit&eacute;, entrez votre adresse ci-dessous.</label>
              <Autocomplete
                  style={{width: '80%'}}
                  className="input"
                  onPlaceSelected={(place) => {
                    var user_address = {
                      address: place.address_components[0].long_name + " " + place.address_components[1].long_name,
                      city: place.address_components[2].long_name,
                      postal_code: place.address_components[6].long_name,
                      latitude: place.geometry.location.lat(),
                      longitude: place.geometry.location.lng(),
                    }
                    dispatch({
                      type: 'CHANGE_ADDRESS',
                      userAddress: user_address
                    })
                    console.log(user_address)
                    localStorage.setItem('userAddress', JSON.stringify(user_address));
                  }}
                  types={['address']}
                  componentRestrictions={{country: "ca"}}
              />
            </div>
            <div className="is-relative is-max-w-3xl mx-auto mb-6 has-text-centered">
              <img src="/metis-assets/elements/pattern-small.png" alt="" style={{maxWidth: "73%"}} />
              <div style={{position:"absolute", top: "3%", left: "11%", width: "77%", height: "90%", textAlign: "center"}}>
                <img style={{objectFit: "cover", width: "80%", height: "100%", borderRadius: "50px"}} src="/imgs/photo-onship-epicerie-accueil.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
        <div className="section">
          <div className="container has-text-centered">
            <h2 className="title">
            Cat&eacute;gories en vedette
            </h2>
            <br/>
            <div className="columns expedier-marche has-text-centered is-multiline is-mobile">
              <div className="column is-half-mobile">
                  <a href="#" className="box">
                    <img src="/imgs/onship-categorie-restaurant.svg" />
                    <br/>
                    <b>Restaurants</b>
                  </a>
              </div>
              <div className="column is-half-mobile">
                  <a href="#" className="box">
                    <img src="/imgs/onship-categorie-epicerie.svg" />
                    <br/>
                    <b>&Eacute;piceries</b>
                  </a>
              </div>
              <div className="column is-half-mobile">
                  <a href="#" className="box">
                    <img src="/imgs/onship-categorie-cafe-patisserie.svg" />
                    <br/>
                    <b>Caf&eacute; & P&acirc;tisseries</b>
                  </a>
              </div>
              <div className="column is-half-mobile">
                  <a href="#" className="box">
                    <img src="/imgs/onship-categorie-fleuriste.svg" />
                    <br/>
                    <b>Fleuriste</b>
                  </a>
              </div>
            </div>
            <br/>
            <h2 className="title">
              Nouveaux commerces sur OnShip.ca
            </h2>
            <br/>
            <div className="columns is-mobile is-multiline home-locations">
              {props.locations.map((location) => (
                <div key={location.id_location} className="column is-half-mobile">
                  <a href={location.slug}>
                      <div className="card location-card">
                        <div className="card-image">
                            <img src={process.env.api_url + location.img_logo} alt={location.name} />
                        </div>
                      </div>
                    </a>
                </div>
              ))}
            </div>
            <p>
              <Link href="/commerces">
                <a className="button is-primary">Voir tous les commerces</a>
              </Link>
            </p>
          </div>
        </div>
        <img src="/imgs/separator.svg" style={{background: "#fbfbfb"}}/>
        <div className="section" style={{background: "#fbfbfb", marginTop: "-7px"}}>
          <div className="container">
            <h2 className="title has-text-centered" style={{marginBottom: "0"}}>
              Nos programmes
            </h2>
            <br/>
            <div className="columns has-text-centered">
              <div className="column">
                  <div className="box">
                    <img style={{maxHeight: "160px"}} src="/imgs/programme-fidelite-onship.ca.svg" />
                    <br/>
                    <b className="title is-size-5">Programme de fid&eacute;lit&eacute;</b>
                    <p>Votre première livraison est gratuite. De plus, chaque fois que vous effectuez 5 commandes, la 6e est gratuite!</p>
                  </div>
              </div>
              <div className="column">
                  <div className="box">
                    <img style={{maxHeight: "160px"}} src="/imgs/onship-consigne-recuperation.svg" />
                    <br/>
                    <b className="title is-size-5">Consignes-tu?</b>
                    <p>Lorsque vous passez une commande, on offre de r&eacute;cup&eacute;rer la consigne et on remet l'argent &agrave; une bonne cause.</p>
                  </div>
              </div>
              <div className="column">
                  <div className="box">
                    <img style={{maxHeight: "160px"}} src="/imgs/onship-programme-personne-mobilite-reduite.svg" />
                    <br/>
                    <b className="title is-size-5">Personnes &agrave; mobilit&eacute; r&eacute;duite</b>
                    <br/><br/>
                    <p>On offre la des rabais sur livraison aux personnes &agrave; mobilit&eacute; r&eacute;duite.</p>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div className="is-hidden" id="sideMenuHeaders01">
          <div className="is-fixed is-top is-right is-fullsize has-background-grey" style={{zIndex: "1"}}></div>
          <nav className="is-fixed is-top is-left is-bottom navbar is-flex is-flex-direction-column pt-4 px-4" style={{width: "80%", height: "100%", overflowY: "scroll"}}>
            <div className="mb-5 navbar-brand is-flex is-justify-content-space-between">
              <a className="navbar-item" href="#"><img style={{minHeight: "35px"}} src="/metis-assets/logos/metis/metis.svg" alt="" width="auto" /></a>
              <a className="navbar-item">
              </a>
            </div>
            <div>
              <nav className="navbar mb-5">
                <div className="is-fullwidth"><a className="navbar-item" href="#">Product</a><a className="navbar-item" href="#">Company</a><a className="navbar-item" href="#">About Us</a><a className="navbar-item" href="#">Features</a></div>
              </nav>
              <div className="mb-6 pt-5 has-border-top"><a className="button is-block is-primary is-outlined mb-2" href="#">Log In</a><a className="button is-block is-primary" href="#">Sign Up</a></div>
            </div>
            <div className="pl-4 pb-4 mt-auto">
              <p className="mb-5 is-size-7">
                <span>Get in Touch</span>
                <a href="#">info@example.com</a>
              </p>
              <a href="#"><img src="/metis-assets/icons/facebook-blue.svg" alt="" /></a><a href="#"><img src="/metis-assets/icons/twitter-blue.svg" alt="" /></a><a href="#"><img src="/metis-assets/icons/instagram-blue.svg" alt="" /></a>
            </div>
          </nav>
        </div>
      </section>
    </div>
  )
}
