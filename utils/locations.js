export function getAllLocations() {
    const res = await fetch(process.env.API_URL + '/api/location/get-all-locations')
    const data = await res.json()
  
    // Returns an array that looks like this:
    // [
    //   {
    //     params: {
    //       id: 'ssg-ssr'
    //     }
    //   },
    //   {
    //     params: {
    //       id: 'pre-rendering'
    //     }
    //   }
    // ]
    return data.locations.map(location => {
      return {
        params: {
          id: location.id_location,
          location: location
        }
      }
    })
  }