export function getAllCities() {
    const res = await fetch(process.env.API_URL + '/api/city/get-all-slug')
    const data = await res.json()

    // Returns an array that looks like this:
    // [
    //   {
    //     params: {
    //       id: 'ssg-ssr'
    //     }
    //   },
    //   {
    //     params: {
    //       id: 'pre-rendering'
    //     }
    //   }
    // ]
    return data.cities.map(city => {
        return {
        params: {
            slug: city.slug,
        }
        }
    })
}
  
export function getCityData(slug) {

    const res = await fetch(process.env.API_URL + '/api/city/get/' + slug)
    const data = await res.json()

    // Combine the data with the id
    return {
        slug,
        ...data.city
    }
}